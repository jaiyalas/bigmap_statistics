{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where
--
-- ---           ----------------------
import           Data.Map.Strict as Map
import           Data.Maybe
-- ---           ----------------------
import           Control.Monad
import           Control.Concurrent
-- ---           ----------------------
import           System.Environment
-- ---           ----------------------
import           Network.HTTP.Simple
import           Network.HTTP.Client
import           Network.Curl
-- ---           ----------------------
import           Data.Aeson
import           Data.Aeson.Types
import qualified Data.ByteString.Lazy as BS
import qualified Data.Text as T
import           Text.Printf (printf)
--
import           BMap
import           Head
--
rest = threadDelay 200000
--
activeLevelH = 1916929

putMyWay :: Show a => Either String [a] -> IO ()
putMyWay (Left s) = putStrLn $ "Left" ++ s
putMyWay (Right []) = return ()
putMyWay (Right (x : xs)) = do
  putStrLn $ show x
  putMyWay (Right xs)

main :: IO ()
main = do
  args <- getArgs
  let [offset, offset_size, threshold] =
        if length args < 3
        then error "not enough args"
        else (Prelude.map read args :: [Int])
  clevel <- getHeadLevel
  --
  let endlevel = clevel - offset
  let startlevel = endlevel - offset_size + 1
  ctx' <- foldM (eachLevel threshold) [] [startlevel .. endlevel]
  let ctx = reverse ctx'
  let allUpdatedKeys = length ctx
  let feqUpdatedKeys = length $ Prelude.filter (\(_,n) -> n >= 1) ctx
  let percentage :: Double =
        (fromIntegral feqUpdatedKeys) / (fromIntegral allUpdatedKeys)
  --
  let filepath =
        "/home/jaiyalas/Project/marigold/bms/output/" ++
        (show offset) ++ "_" ++
        "l" ++ (show offset_size) ++ "_" ++
        "g" ++ (show threshold) ++ "_" ++
        "p" ++ (printf "%.3f" percentage)++
        ".csv"
  writeFile filepath $ toCSVString ctx
  --

eachLevel :: Int -> [(Int, Int)] -> Int -> IO [(Int, Int)]
eachLevel threshold hs lv = do
  rest
  m <- getBigMapKeyUpdatesMap lv
  case m of
    Left _ -> return hs
    Right m ->
      return (( lv
              , Map.size $ Map.filter (> threshold) m) : hs)

toCSVString :: [(Int,Int)] -> String
toCSVString [] = ""
toCSVString ((lv, n) : xs) =
  (show lv) ++ ", " ++
  (show n) ++ "\n" ++
  toCSVString xs
