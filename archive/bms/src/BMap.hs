{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
--
module BMap where
--
import           Control.Monad
-- ---           ----------------------
import           Data.Map.Strict as Map
-- ---           ----------------------
import           Network.HTTP.Simple
import           Network.HTTP.Client
import           Network.Curl
-- ---           ----------------------
import           Data.Aeson
import           Data.Aeson.Types
-- ---           ----------------------
--
url_bigmap_update lv =
  "https://api.tzkt.io/v1/bigmaps/updates" ++
  "?level=" ++ (show lv) ++
  "&limit=10000"


url_bigmap_key_update lv =
  (url_bigmap_update lv) ++ "&action.eq=update_key"


type BigMapPtr = Int
type KeyHash = String
type Level = Int
type UpdateMap = Map (BigMapPtr,KeyHash) Int


data BigMapUpdate = BigMapUpdate
  { bmu_id :: Int
  , bmu_lv :: Level
  , bmu_ptr :: BigMapPtr
  , bmu_khash :: Maybe KeyHash
  } deriving (Show, Eq)

instance FromJSON BigMapUpdate where
  parseJSON = withObject "" $ \o -> do
    p1 <- o .:  "id"
    p2 <- o .:  "level"
    p3 <- o .:  "bigmap"
    p4 <- (o .:? "content")
    p4' <- case p4 of
             Nothing -> return Nothing
             Just p -> withObject "" (.:? "hash") p
    return $ BigMapUpdate p1 p2 p3 p4'

getBigMapKeyUpdates :: Level -> IO (Either String [BigMapUpdate])
getBigMapKeyUpdates lv = do
  (cc, bstr) <- curlGetString_ (url_bigmap_key_update lv) []
  return $ eitherDecode bstr

getBigMapKeyUpdatesMap :: Level -> IO (Either String UpdateMap)
getBigMapKeyUpdatesMap lv = do
  res <- getBigMapKeyUpdates lv
  case res of
    Left str -> return $ Left str
    Right xs -> return $ Right $ trans xs

trans :: [BigMapUpdate] -> UpdateMap
trans [] = Map.empty
trans (x:xs) =
  case bmu_khash x of
    Nothing -> trans xs
    Just kh -> Map.insertWith (+) (bmu_ptr x, kh) 1 (trans xs)

-- # bigmap count

url_bigmap_count = "https://api.tzkt.io/v1/bigmaps/count"

getBigMapCount :: IO (Either String Int)
getBigMapCount = do
  (cc, bstr) <- curlGetString_ url_bigmap_count []
  return $ eitherDecode bstr

-- # bigmaps

url_bigmaps startLv =
  "https://api.tzkt.io/v1/bigmaps"++
  "?level.ge=" ++ (show startLv)  ++
  "&limit=10000"

data BigMap = BigMap
  { ptr :: Int
  , bmpath :: Maybe String
  , firstLevel :: Int
  , lastLevel :: Int
  , totalKeys :: Int
  , activeKeys ::Int
  , updates :: Int
  } deriving (Show, Eq)

instance FromJSON BigMap where
  parseJSON = withObject "" $ \o -> do
    p1 <- o .:  "ptr"
    p2 <- o .:? "path"
    l1 <- o .:  "firstLevel"
    l2 <- o .:  "lastLevel"
    k1 <- o .:  "totalKeys"
    k2 <- o .:  "activeKeys"
    u  <- o .:  "updates"
    return $ BigMap p1 p2 l1 l2 k1 k2 u

getBigMaps :: Int -> IO (Either String [BigMap])
getBigMaps slv = do
  (cc, bstr) <- curlGetString_ (url_bigmaps slv) []
  return $ eitherDecode bstr

