{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
--
module BMap where
--
import           Control.Monad
-- ---           ----------------------
import           Data.Map.Strict as Map
import qualified Data.Set as Set
-- ---           ----------------------
import           Network.HTTP.Simple
import           Network.HTTP.Client
import           Network.Curl
-- ---           ----------------------
import           Data.Aeson
import           Data.Aeson.Types
-- ---           ----------------------
--
type BigMapPtr = Int
type KeyHash = String
type Level = Int
--
type UpdateMap = Map (BigMapPtr, KeyHash) Int
type BigMapSet = Set.Set BigMapPtr
--
toUpdateMap :: [BigMapUpdate] -> UpdateMap
toUpdateMap [] = Map.empty
toUpdateMap (x:xs) =
  case bmu_khash x of
    Nothing -> toUpdateMap xs
    Just kh -> Map.insertWith (+) (bmu_ptr x, kh) 1 (toUpdateMap xs)
--

{- ##### accessor ##### -}

-- | returns the total number of bigmaps.
getBigMapCount :: IO (Either String Int)
getBigMapCount = do
  (cc, bstr) <- curlGetString_ url_bigmap_count []
  return $ eitherDecode bstr

-- | returns all the big_maps updated greater or equals to given level.
getBigMaps :: Int -> IO (Either String [BigMap])
getBigMaps slv = do
  (cc, bstr) <- curlGetString_ (url_bigmaps slv) []
  return $ eitherDecode bstr

-- | returns a set of all the big_maps who is updated
--   greater or equals to the given level.
getBigMapSet :: Level -> IO (Either String BigMapSet)
getBigMapSet lv = do
  res <- getBigMapUpdates lv
  case res of
    Left str -> return $ Left str
    Right xs -> return $ Right $
      Set.fromList $ Prelude.map bmu_ptr $ xs

-- | returns all updates for the given level
getBigMapUpdates :: Level -> IO (Either String [BigMapUpdate])
getBigMapUpdates lv = do
  (cc, bstr) <- curlGetString_ (url_bigmap_update lv) []
  return $ eitherDecode bstr

-- | returns all updates for the given level
getBigMapUpdatesMap :: Level -> IO (Either String UpdateMap)
getBigMapUpdatesMap lv = do
  res <- getBigMapUpdates lv
  case res of
    Left str -> return $ Left str
    Right xs -> return $ Right $ toUpdateMap xs

-- | returns all "key_update" updates for the given level
getBigMapUpdates_keyupdate :: Level -> IO (Either String [BigMapUpdate])
getBigMapUpdates_keyupdate lv = do
  (cc, bstr) <- curlGetString_ (url_bigmap_update_keyupdate lv) []
  return $ eitherDecode bstr

-- | returns a map of all "key_update" updates for the given level
getBigMapUpdatesMap_keyupdate :: Level -> IO (Either String UpdateMap)
getBigMapUpdatesMap_keyupdate lv = do
  res <- getBigMapUpdates_keyupdate lv
  case res of
    Left str -> return $ Left str
    Right xs -> return $ Right $ toUpdateMap xs

{- ##### JSON parser ##### -}

-- | big map
data BigMap = BigMap
  { ptr :: Int
  , bmpath :: Maybe String
  , firstLevel :: Int
  , lastLevel :: Int
  , totalKeys :: Int
  , activeKeys ::Int
  , updates :: Int
  } deriving (Show, Eq)

instance FromJSON BigMap where
  parseJSON = withObject "" $ \o -> do
    p1 <- o .:  "ptr"
    p2 <- o .:? "path"
    l1 <- o .:  "firstLevel"
    l2 <- o .:  "lastLevel"
    k1 <- o .:  "totalKeys"
    k2 <- o .:  "activeKeys"
    u  <- o .:  "updates"
    return $ BigMap p1 p2 l1 l2 k1 k2 u

-- | big map update
data BigMapUpdate = BigMapUpdate
  { bmu_id :: Int
  , bmu_lv :: Level
  , bmu_ptr :: BigMapPtr
  , bmu_khash :: Maybe KeyHash
  } deriving (Show, Eq)

instance FromJSON BigMapUpdate where
  parseJSON = withObject "" $ \o -> do
    p1 <- o .:  "id"
    p2 <- o .:  "level"
    p3 <- o .:  "bigmap"
    p4 <- (o .:? "content")
    p4' <- case p4 of
             Nothing -> return Nothing
             Just p -> withObject "" (.:? "hash") p
    return $ BigMapUpdate p1 p2 p3 p4'

{- ##### urls ##### -}
--
--
url_bigmap_count =
  "https://api.tzkt.io/v1/bigmaps/count"
--
url_bigmaps lv =
  "https://api.tzkt.io/v1/bigmaps"++
  "?lastLevel.ge=" ++ (show lv)  ++
  "&limit=10000"
--
url_bigmap_update lv =
  "https://api.tzkt.io/v1/bigmaps/updates" ++
  "?level.eq=" ++ (show lv) ++
  "&limit=10000"
--
url_bigmap_update_keyupdate lv =
  (url_bigmap_update lv) ++
  "&action.in=add_key,update_key"
--
