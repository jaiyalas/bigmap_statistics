{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where
--
-- ---           ----------------------
import qualified Data.Map.Strict as Map
import           Data.Maybe
import qualified Data.Set as Set
import           Data.String
-- ---           ----------------------
import           Control.Monad
import           Control.Concurrent
-- ---           ----------------------
import           System.Environment
-- ---           ----------------------
import           Network.HTTP.Simple
import           Network.HTTP.Client
import           Network.Curl
-- ---           ----------------------
import           Data.Aeson
import           Data.Aeson.Types
import qualified Data.ByteString.Lazy as BS
import qualified Data.Text as T
import           Text.Printf (printf)
--
import           BMap
import           Head
--
rest = threadDelay 200000
--
activeLevel_H = 1916929
baseLevel     = 2100000
--
main :: IO ()
main = do
  args <- getArgs
  let [offset, offset_size, threshold] =
        if length args < 3
        then error "[not enough args@main]"
        else (Prelude.map read args :: [Int])
  currentLevel <- getHeadLevel
  --
  -- let endlevel = currentLevel - offset
  -- let startlevel = endlevel - offset_size + 1
  let startlevel = baseLevel - offset
  let endlevel = startlevel + offset_size
  let levels = [startlevel .. endlevel]
  --
  let k = eachLevel
  --
  results <- mapM k levels
  --
  let rs = foo threshold $ map fst results
  let ctx =
        unlines
        $ map ppyy
        $ zip levels
        $ zip (map Map.size rs)
        $ map snd results
  -- putStrLn ctx
  --
  let filepath =
        "/home/jaiyalas/Project/marigold/bigmap_statistics/bms3/output/" ++
        (show startlevel) ++ "_" ++
        "l" ++ (show offset_size) ++ "_" ++
        "w" ++ (show threshold) ++ "_" ++
        ".csv"
  writeFile filepath ctx
  --

ppyy :: (Int, (Int, Int)) -> String
ppyy (l, (f, t)) =
  let p :: Double = (fromIntegral f) / (fromIntegral t) in
  (show l)
  ++ ", " ++
  (show f)
  ++ ", " ++
  (show t)
  ++ ", " ++
  (printf "%.3f" p)
--
eachLevel :: Int -> Level -> IO (UpdateMap, Int)
eachLevel 0 lv =
  return (Map.empty, 0)
eachLevel count lv = do
  rest
  m  <- getBigMapUpdatesMap lv
  case m of
    Left e -> do
      rest
      rest
      eachLevel (count - 1) lv
    Right xs -> do
      let s = Map.size xs
      return (xs, s)
--
foo :: Int -> [UpdateMap] -> [Map.Map (BigMapPtr, KeyHash) (Int, Int)]
foo window [] = []
foo window (m : ms) =
  (flip (:)) (foo window ms)
  $ Map.intersectionWith (\n m -> (n, m)) m
  $ Map.unionsWith (+)
  $ take window ms
--
toCSV :: Show a => [[a]] -> String
toCSV = unlines . map (foldr (\x hs-> (show x) ++ ", " ++ hs) "")
--
printEitherListLn :: (Show a, Show b) => Either a [b] -> IO ()
printEitherListLn (Left a) = putStrLn $ "[Left] " ++ (show a)
printEitherListLn (Right xs) =
  putStrLn $ unlines $ map show xs
