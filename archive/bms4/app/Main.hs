{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where
--
-- ---           ----------------------
import qualified Data.Map.Strict as Map
import           Data.Maybe
import qualified Data.Either as Either
import qualified Data.Set as Set
import           Data.String
-- ---           ----------------------
import           Control.Monad
import           Control.Concurrent
-- ---           ----------------------
import           System.Environment
-- ---           ----------------------
import           Network.HTTP.Simple
import           Network.HTTP.Client
import           Network.Curl
-- ---           ----------------------
import           Data.Aeson
import           Data.Aeson.Types
import qualified Data.ByteString.Lazy as BS
import qualified Data.Text as T
import           Text.Printf (printf)
-- ---           ----------------------
import           BMap
import           Head
import           Block
import           Types
-- ---           ----------------------
--
rest = threadDelay 200000
--
activeLevel_H = 1916929
baseLevel     = 2100000
--

f1 = "contracts_2120000_2120999"
f2 = "contracts_2121000_2121999"
f3 = "contracts_2122000_2122999"
f4 = "contracts_2123000_2123999"
f5 = "contracts_2124000_2124999"
startlv = 2120000
endlv = 2124999
--
period ge le =
  "level.ge="++(show ge)++"&level.le=" ++ (show le)
--
period_all = period 2120000 2124999
period_1 = period 2120000 2120999
period_2 = period 2121000 2121999
period_3 = period 2122000 2122999
period_4 = period 2123000 2123999
period_5 = period 2124000 2124999
--

read2Set :: String -> IO (Set.Set String)
read2Set f = do
  let inpath =
        "/home/jaiyalas/Project/marigold/" ++
        "bigmap_statistics/bms4/output/" ++
        f
  contracts_str <- readFile inpath
  let contracts = (read contracts_str) :: [String]
  return $ Set.fromList contracts

main :: IO ()
main = do
  putStrLn "[level]: 2120000 -- 2124999 (5000 levels > 40 hrs)"
  s1 <- read2Set f1
  -- putStrLn $ show $ Set.size s1
  s2 <- read2Set f2
  -- putStrLn $ show $ Set.size s2
  s3 <- read2Set f3
  -- putStrLn $ show $ Set.size s3
  s4 <- read2Set f4
  -- putStrLn $ show $ Set.size s4
  s5 <- read2Set f5
  -- putStrLn $ show $ Set.size s5
  --
  -- get all active contracts
  let ss = foldr Set.union Set.empty [s1,s2,s3,s4,s5]
  putStr "[total contract]: "
  putStrLn $ show $ Set.size ss
  -- setup level periods
  let f c = concat
        [ "https://api.tzkt.io/v1/bigmaps/updates"
        , "?contract=" ++ c
        , "&"++period_all
        ]
  --
  l <- getTxNum "KT1AFq5XorPduoYyWxs5gEyrFK6fVjJVbtCj"
  putStrLn $ "KT1AFq5XorPduoYyWxs5gEyrFK6fVjJVbtCj = " ++ (show l)

data TxUnit = TxUnit deriving (Show,Eq)

instance FromJSON TxUnit where
  parseJSON = withObject "" $ \o -> do
    (_p :: String) <- (o .:  "type")
    return TxUnit

getTxNum :: Contract -> IO Int
getTxNum contract = liftM sum $ mapM (getTxNum_period contract)
    [period_1, period_2, period_3, period_4, period_5]

getTxNum_period :: String -> String -> IO Int
getTxNum_period contract period_n = do
  let url = concat
            [ "https://api.tzkt.io/v1/operations/transactions"
            , "?target=" ++ contract
            , "&"++period_n
            , "&status=applied"
            , "&limit=5000"
            ]
  (cc, bstr) <- curlGetString_ url []
  case eitherDecode bstr of
    Left msg -> do
      putStrLn msg
      return 0
    Right (xs :: [TxUnit]) -> return $ length xs

  --
  --
  --
  -- let filepath =
  --       "/home/jaiyalas/Project/marigold/bigmap_statistics/bms4/output/" ++
  --       "contracts_" ++
  --       (show startlevel) ++ "_" ++ (show endlevel)
  -- writeFile filepath $ show $ Set.toList contractSet
  --
  -- putStrLn "[end]"
      -- --
      -- let rs = foo threshold $ map fst results
      -- let ctx =
      --       unlines
      --       $ map ppyy
      --       $ zip levels
      --       $ zip (map Map.size rs)
      --       $ map snd results
      -- -- putStrLn ctx
      -- --
      -- let filepath =
      --       "/home/jaiyalas/Project/marigold/bigmap_statistics/bms4/output/" ++
      --       (show startlevel) ++ "_" ++
      --       "l" ++ (show offset_size) ++ "_" ++
      --       "w" ++ (show threshold) ++ "_" ++
      --       ".csv"
      -- writeFile filepath ctx
  --

-- https://api.tzkt.io/v1/operations/transactions
-- ?level=
-- &target=

data ContractBigMapStat =
  CBMS
  { tnum_bigmap :: Int
  , tnum_actkey :: Int
  , tnum_ttlkey :: Int
  } deriving (Show, Eq, Ord)

foooo :: Contract -> IO ContractBigMapStat
foooo contract = do
  set <- getBMPs contract
  let aks = Set.foldr' (\s n-> activeKeys s + n) 0 set
      tks = Set.foldr' (\s n-> totalKeys s + n) 0 set
  return $ CBMS (Set.size set) aks tks

getBMPs :: Contract -> IO (Set.Set BigMap)
getBMPs contract = do
  let url =
        "https://api.tzkt.io/v1/bigmaps"++
        "?contract=" ++ (show contract)
  (cc, bstr) <- curlGetString_ url []
  return $ Set.fromList $ Either.fromRight [] $ eitherDecode bstr


ppyy :: (Int, (Int, Int)) -> String
ppyy (l, (f, t)) =
  let p :: Double = (fromIntegral f) / (fromIntegral t) in
  (show l)
  ++ ", " ++
  (show f)
  ++ ", " ++
  (show t)
  ++ ", " ++
  (printf "%.3f" p)

eachLevel :: Level -> IO (UpdateMap, Int)
eachLevel lv = do
  rest
  m  <- getBigMapUpdatesMap lv
  case m of
    Left e -> do
      print e
      error "[error@eachLevel]"
    Right xs -> do
      let s = Map.size xs
      -- putStrLn $ "Lv("++(show lv)++")size:" ++ show s
      return (xs, s)
--

foo :: Int -> [UpdateMap] -> [Map.Map (BigMapPtr, KeyHash) (Int, Int)]
foo window [] = []
foo window (m : ms) =
  (flip (:)) (foo window ms)
  $ Map.intersectionWith (\n m -> (n, m)) m
  $ Map.unionsWith (+)
  $ take window ms
--
toCSV :: Show a => [[a]] -> String
toCSV = unlines . map (foldr (\x hs-> (show x) ++ ", " ++ hs) "")
--
printEitherListLn :: (Show a, Show b) => Either a [b] -> IO ()
printEitherListLn (Left a) = putStrLn $ "[Left] " ++ (show a)
printEitherListLn (Right xs) =
  putStrLn $ unlines $ map show xs
--
wordsWhen :: (Char -> Bool) -> String -> [String]
wordsWhen p s =
  case dropWhile p s of
    "" -> []
    s' -> w : wordsWhen p s''
      where (w, s'') = break p s'

{-

  args <- getArgs
  let inputname = -- [baseLevel, offset, inputname] =
        (Prelude.head args :: String)
  -- currentLevel <- getHeadLevel
  --
  -- let endlevel = currentLevel - offset
  -- let startlevel = endlevel - offset_size + 1
  let [_,l0str,l1str] = wordsWhen (== '_') inputname
  let startlevel = (read l0str :: Int)
  let endlevel = (read l1str :: Int)
  putStrLn $ "level: " ++ (show startlevel) ++ " - " ++ (show endlevel)
  --
  -- let levels = [startlevel .. endlevel]
  -- css <- mapM getActiveContract levels
  -- let contractSet =
  --       foldr
  --       (\cs hs -> Set.union (Set.fromList cs) hs)
  --       Set.empty
  --       css

-}
