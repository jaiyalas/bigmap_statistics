{-# LANGUAGE OverloadedStrings #-}

module Main where

import           BmsInDB
import           BmsLib
import           Options.Applicative

main :: IO ()
main = do
  opts <- execParser args
  case opts of
    Fetch_bmu db s e o -> do
      runDb
        db
        toQ
        (getBigMapUpdatesRange (fromInteger s) (fromInteger e) (fromInteger o))
    Fetch_cc db s e o -> do
      runDb_cc
        db
        toQ_ccall
        (getContractCallRange (fromInteger s) (fromInteger e) (fromInteger o))
    Fetch_bm db o -> do
      runDb_bigmap
        db
        toQ_bigmap
        (getBigmapRange (fromInteger o))
    Simu db s e -> do
      xs <- select_ccall
            db
            (fromInteger s)
            (fromInteger e)
      putStrLn $ show $ length xs
      -- pp $ head xs

