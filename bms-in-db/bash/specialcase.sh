#!/bin/bash

parampsql='host=0.0.0.0 dbname=txtable user=postgres port=5434'

sql_select="SELECT COUNT(DISTINCT(hash))"

sql_from="FROM contract_calls"

oneweek="2127600"
twoweek="2107440"

sql_where="WHERE target LIKE 'KT1%' AND level >= $oneweek"

sql_extra=""

sql="$sql_select $sql_from $sql_where $sql_extra"

echo -e "[\e[0;34mstart\e[0m]"
echo -e "[\e[1;33mstart level\e[0m]"
psql "$parampsql" -c "SELECT MIN(level) $sql_from" | sed 's/^/\t/'
echo -e "[\e[1;33mend level\e[0m]"
psql "$parampsql" -c "SELECT MAX(level) $sql_from" | sed 's/^/\t/'
# => 2147760

# 1 week  = 20160 (level) -> 2147760 - 20160 = 2127600
# 2 weeks = 40320 (level) -> 2147760 - 40320 = 2107440

echo -e "[\e[1;33mrecent calls within 2 weeks \e[0m] (\e[0;36m$sql\e[0m)"
psql "$parampsql" -c "$sql" | sed 's/^/\t/'

echo -e "[\e[0;34mend\e[0m]"





# 
# list=(511 6072 513 22785 22788 22789 5627 5909 22799 75550 861 6210 3920 3921 3916 3919 103258 55542 523 22381 5629 9923 36518 77034 6809 4874 11230 1772 75634 3669 12043 3686 42519 24157 9940 54249 12112 39478 38108 70074 88080 23775)
# 
# for i in  ${list[@]};do
#     echo "bigmap_id=$i"
#     
# done
# 
# 
# 
# 
# SELECT COUNT(DISTINCT(diff_content_keyhash)) FROM contract_calls WHERE target = 'KT1KEa8z6vWXDJrVqtMrAeDVzsvxat3kHaCE' AND diff_action = 'update_key' AND diff_bigmap = 22788;
