/*
https://api.tzkt.io/v1/bigmaps
    ?active=true
    &sort=ptr
    &select=ptr
           ,contract
           ,firstLevel
           ,lastLevel
           ,totalKeys
           ,activeKeys
           ,updates
           ,keyType
           ,valueType
*/

create table if not exists bigmaps (
  id serial primary key,
  /* --- */
  ptr integer,
  contract text,
  /* --- */
  firstLevel integer,
  lastLevel integer,
  /* --- */
  totalKeys integer,
  activeKeys integer,
  updates integer,
  /* --- */
  keyType text,
  valueType text
);

create index if not exists idx_ptr on bigmaps(ptr);
create index if not exists idx_contract on bigmaps(contract);
create index if not exists idx_firstLevel on bigmaps(firstLevel);
create index if not exists idx_lastLevel on bigmaps(lastLevel);
/* --- */
create index if not exists idx_totalKeys on bigmaps(totalKeys);
create index if not exists idx_activeKeys on bigmaps(activeKeys);
create index if not exists idx_updates on bigmaps(updates);
