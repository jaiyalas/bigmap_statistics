create table if not exists bigmap_updates (
  id serial primary key,
  bmp_id integer,
  level integer,
  pointer integer,
  action text,
  address text,
  hash text
);

create index if not exists idx_level on bigmap_updates(level);
create index if not exists idx_pointer on bigmap_updates(pointer);
create index if not exists idx_action on bigmap_updates(action);
create index if not exists idx_address on bigmap_updates(address);
create index if not exists idx_hash on bigmap_updates(hash);
