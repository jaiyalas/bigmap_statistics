/*
https://api.tzkt.io/v1/operations/transactions
   ?target=KT1AFq5XorPduoYyWxs5gEyrFK6fVjJVbtCj
   &level.ge=2123000
   &level.le=2123999
   &status=applied
   &limit=5000
   &select=level
          ,hash
          ,sender
          ,target
          ,parameter
          ,gasUsed
          ,storageUsed
          ,storageFee
          ,allocationFee
          ,diffs
*/

create table if not exists contract_calls (
  id serial primary key,
  /* --- */
  level integer,
  hash text,
  sender text,
  target text,
  /* --- */
  param_entrypoint text,
  param_value text,
  /* --- */
  gasUsed integer,
  storageUsed integer,
  storageFee integer,
  allocationFee integer,
  /* --- */
  diff_bigmap integer,
  diff_path text,
  diff_action text,
  /* --- */
  diff_content_keyhash text,
  diff_content_key text,
  diff_content_value text
);

create index if not exists idx_level on contract_calls(level);
create index if not exists idx_hash on contract_calls(hash);
create index if not exists idx_sender on contract_calls(sender);
create index if not exists idx_target on contract_calls(target);
/* --- */
create index if not exists idx_entrypoint on contract_calls(param_entrypoint);
create index if not exists idx_diff_bigmap on contract_calls(diff_bigmap);
create index if not exists idx_diff_action on contract_calls(diff_action);
create index if not exists idx_diff_keyhash on contract_calls(diff_content_keyhash);
