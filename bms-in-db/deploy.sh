#!/bin/bash

echo ""
echo "create new DB instance"
docker run -p 5434:5432 --name bmpInDB -e POSTGRES_PASSWORD=pw -e POSTGRES_DB=bigmap -d postgres:13.5-alpine

sleep 5

PASSWORD=pw psql "host=0.0.0.0 dbname=bigmap user=postgres port=5434" -f ./config/create_table.sql
