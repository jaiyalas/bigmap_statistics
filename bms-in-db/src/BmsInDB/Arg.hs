{-# LANGUAGE OverloadedStrings #-}

module BmsInDB.Arg where

import           Options.Applicative

data Arg
  = Fetch_bmu -- | bigmap updates
    { f_db :: String
    , startLevel :: Integer
    , endLevel :: Integer
    , offset :: Integer
    }
  | Fetch_cc -- | contract call (tx)
    { cc_db :: String
    , cc_startLevel :: Integer
    , cc_endLevel :: Integer
    , cc_offset :: Integer
    }
  | Fetch_bm -- | bigmap
    { bm_db :: String
    , bm_offset :: Integer
    }
  | Simu -- | simulation for 2-layer design
    { s0_db :: String
    , s0_startLevel :: Integer
    , s0_endLevel :: Integer
    }

args :: ParserInfo Arg
args = flip info fullDesc
  $ subparser (c1 <> c2 <> c3 <> c4)
  where
    c1 =
      command "Fetch"
      (info
        (fetchParser <**> helper)
        (progDesc "fetch bigmap update and store into db"))
    c2 =
      command "Fetchcc"
      (info
        (ccParser <**> helper)
        (progDesc "fetch contract call and store into db"))
    c3 =
      command "Fetchbm"
      (info
        (bmParser <**> helper)
        (progDesc "fetch bigmap and store into db"))
    c4 =
      command "Simulate"
      (info
        (simuParser <**> helper)
        (progDesc "simulate 2-layer cache"))

fetchParser :: Parser Arg
fetchParser = Fetch_bmu
  <$> dbArg
  <*> levelArg "start level" 's'
  <*> levelArg "end level" 'e'
  <*> offsetArg

ccParser :: Parser Arg
ccParser = Fetch_cc
  <$> dbArg
  <*> levelArg "start level" 's'
  <*> levelArg "end level" 'e'
  <*> offsetArg

bmParser :: Parser Arg
bmParser = Fetch_bm
  <$> dbArg
  <*> offsetArg

simuParser :: Parser Arg
simuParser = Simu
  <$> dbArg
  <*> levelArg "start level" 's'
  <*> levelArg "end level" 'e'

levelArg :: String -> Char -> Parser Integer
levelArg desc s = option auto (long desc <> short s <> help desc)

offsetArg :: Parser Integer
offsetArg = option auto (long "offset" <> short 'o' <> help "offset")

dbArg :: Parser String
dbArg = strOption
  (long "db_path"
   <> short 'd'
   <> metavar "DB_PATH"
   <> help "the path of sqlite database")
