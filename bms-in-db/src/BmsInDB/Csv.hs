{-# LANGUAGE OverloadedStrings #-}

module BmsInDB.Csv where

-- import qualified Data.Text as T
import System.IO
import Data.String

decodeCSV :: String -> [String]
decodeCSV str =
  case break (==',') str of
    (a, ',':b) -> a : decodeCSV b
    (a, "")    -> [a]

loadCSV :: FilePath -> IO [[String]]
loadCSV fp = readFile fp >>= return . (fmap decodeCSV) . lines

-- loadCSV_T :: Filepath -> IO [[T.Text]]
-- loadCSV_T fp = loadCSV fp >>= liftM (fmap T.pack)
