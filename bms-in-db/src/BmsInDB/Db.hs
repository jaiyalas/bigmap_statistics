{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module BmsInDB.Db where

import           BmsLib hiding (diff_bigmap, diff_action, ptr)
import           Data.Aeson
import qualified Data.Map as Map
import qualified Database.PostgreSQL.Simple as DS
import qualified Data.ByteString.Char8 as BC
import           GHC.Int (Int64)
import           GHC.Generics (Generic)

runDb :: String -> DS.Query -> (([BigMapUpdate] -> IO Int64) -> IO ()) -> IO ()
runDb db q f = do
  conn <- DS.connectPostgreSQL $ BC.pack db
  f $ DS.executeMany conn q
  DS.close conn

runDb_cc :: String -> DS.Query -> (DBWriter ContractCallRow -> IO ()) -> IO ()
runDb_cc db q dbk = do
  conn <- DS.connectPostgreSQL $ BC.pack db
  dbk $ DS.executeMany conn q
  DS.close conn

runDb_bigmap :: String -> DS.Query -> (DBWriter BigmapRow -> IO ()) -> IO ()
runDb_bigmap db q dbk = do
  conn <- DS.connectPostgreSQL $ BC.pack db
  dbk $ DS.executeMany conn q
  DS.close conn

data Bigmap_diff = Bigmap_diff
  { level :: Int
  , hash :: String
  , target :: String
  , diff_bigmap :: Maybe Int
  , diff_action :: Maybe String
  , diff_content_keyhash :: Maybe String
  } deriving (Show, Eq, Ord, Generic)

data Akeys = Akeys
  { ptr :: Int
  , contract :: String
  , activekeys :: Int
  } deriving (Show, Eq, Ord, Generic)

instance DS.FromRow Bigmap_diff
instance DS.FromRow Akeys

type AKMap = Map.Map Int Int

toAKMap :: [Akeys] -> AKMap
toAKMap [] = Map.empty
toAKMap (ks : kss) =
  Map.insert (ptr ks) (activekeys ks) (toAKMap kss)

select_ccall :: String -> Integer -> Integer -> IO ([Bigmap_diff], AKMap)
select_ccall db s e = do
  conn <- DS.connectPostgreSQL $ BC.pack db
  xs <- DS.query conn select_ccall_query $ (s, e)
  ks <- mapM (\x -> select_akey conn (diff_bigmap x)) xs
  DS.close conn
  return (xs, toAKMap ks)

select_akey :: DS.Connection -> Maybe Int -> IO Akeys
select_akey conn (Just ptr) = do
  xs <- DS.query conn select_bigmap_akey $ (DS.Only ptr)
  return $ head xs
select_akey _ Nothing = undefined

select_ccall_query :: DS.Query
select_ccall_query =
  "SELECT level, hash, target, diff_bigmap, diff_action, diff_content_keyhash FROM contract_calls \
  \WHERE target LIKE 'KT%'\
  \AND level >= ?\
  \AND level <= ?\
  \AND diff_bigmap IS NOT NULL\
  \;"

select_bigmap_akey :: DS.Query
select_bigmap_akey =
  "SELECT ptr,contract,activekeys \
  \FROM bigmaps \
  \WHERE ptr=?\
  \;"

toQ :: DS.Query
toQ = "INSERT INTO bigmap_updates (\
      \bmp_id, level, pointer, action, address, hash\
      \) VALUES (\
      \?,?,?,?,?,?\
      \)"

toQ_ccall :: DS.Query
toQ_ccall = "INSERT INTO contract_calls (\
            \level, hash, sender, target,\
            \param_entrypoint, param_value,\
            \gasUsed, storageUsed, storageFee, allocationFee,\
            \diff_bigmap, diff_path, diff_action,\
            \diff_content_keyhash, diff_content_key, diff_content_value\
            \) VALUES (\
            \?,?,?,?,\
            \?,?,\
            \?,?,?,?,\
            \?,?,?,\
            \?,?,?\
            \)"

toQ_bigmap :: DS.Query
toQ_bigmap = "INSERT INTO bigmaps (\
             \ptr, contract,\
             \firstLevel, lastLevel,\
             \totalKeys, activeKeys, updates,\
             \keyType, valueType\
             \) VALUES (\
             \?,?,\
             \?,?,\
             \?,?,?,\
             \?,?\
             \)"
