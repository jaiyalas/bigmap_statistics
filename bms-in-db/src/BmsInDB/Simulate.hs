{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
--
module BmsInDB.Simulate where
--
import           BmsLib hiding (diff_bigmap, diff_action)
--
import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified Data.Vector as Vec
import qualified Data.ByteString.Char8 as BC
--
import           GHC.Int (Int64)
import           GHC.Generics (Generic)
--

-- type MapId = Int
-- type Key = String
-- type TotalAKeys = Int
-- 
-- data Cache a =
--   Cache
--   { content :: Vec.Vector a
--   , maxSize :: Int
--   , usedSize :: Int
--   }
-- 
-- class Sized a where
--   size_of :: a -> Int
-- 
-- 
-- dropPolicy :: Sized a => a -> Cache a -> Cache a
-- dropPolicy x c
--   | (usedSize c) + (size_of x) > (maxSize c) =
--     let Just (vec, last) = Vec.unsnoc $ content c
--     in dropPolicy x (Cache { content = vec
--                            , maxSize = maxSize c
--                            , usedSize = (usedSize c) + (size_of last)})
--   | otherwise = c
-- 
-- insert :: Sized a => a -> Cache a -> Cache a
-- insert x c = Vec.cons x (dropPolicy x c)


-- 
-- 
-- 
-- 
-- 
-- data DualCache = DualCache
--   { l0 :: Cache
--   , l1 :: Cache
--   } deriving (Show, Eq, Ord)
-- 
-- 
-- 
-- 
-- 
-- 
-- 
-- 
-- insertRecord :: MapId -> Key -> TotalAKeys -> DualCache -> DualCache
-- insertRecord mid k taks cache =
--   let cache0 = l0 cache
--       cache1 = l1 cache
--       map0 = content (l0 cache)
--   in case Map.lookup mid map0 of
--     Nothing -> undefined
--     Just (ks, taks) -> 
--     
--   then if Set.meber k Map.lookup map0
--     let xx = Set.
--   else (Set.singleton k,taks)
-- 
--     Map.insert (\n o -> Set.) mid (Set, taks) map0
-- 
-- 
--     undefined
--  --  DualCache (cache0 {content = Map.insert mid (content cache0)}) cache1
-- 
-- 
-- 
-- relocate :: MapId -> DualCache -> DualCache
-- relocate mid dc =
--   case Map.lookup mid $ content (l0 dc) of
--     Nothing -> dc
--     Just (set, taks) -> DualCache
--       { l0 = Cache
--              { content = Map.delete mid $ content (l0 dc)
--              , maxSize = maxSize $ l0 dc
--              , usedSize = (usedSize (l0 dc)) - (Set.size set)
--              }
--       , l1 = Cache
--              { content = Map.insert mid (Set.empty, taks) $ content (l1 dc)
--              , maxSize = maxSize $ l1 dc
--              , usedSize = (usedSize (l1 dc)) + taks
--              }
--       }
--
--
-- data Bigmap_diff = Bigmap_diff
--   { level :: Int
--   , hash :: String
--   , target :: String
--   , diff_bigmap :: Maybe Int
--   , diff_action :: Maybe String
--   , diff_content_keyhash :: Maybe String
--   } deriving (Show, Eq, Ord, Generic)
--
-- data Akeys = Akeys
--   { ptr :: Int
--   , contract :: String
--   , activekeys :: Int
--   } deriving (Show, Eq, Ord, Generic)
--
-- instance DS.FromRow Bigmap_diff
-- instance DS.FromRow Akeys
--
-- type AKMap = Map.Map Int Int
--
-- toAKMap :: [Akeys] -> AKMap
-- toAKMap [] = Map.empty
-- toAKMap (ks : kss) =
--   Map.insert (ptr ks) (activekeys ks) (toAKMap kss)
