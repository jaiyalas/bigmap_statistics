{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

--
module BmsLib.BMap where

--
import           Control.Monad
-- ---           ----------------------
import           Data.Map.Strict as Map
import qualified Data.Set as Set
-- ---           ----------------------
import           Network.HTTP.Simple
import           Network.HTTP.Client
import           Network.Curl
-- ---           ----------------------
import           Data.Aeson
import           Data.Aeson.Types
import qualified Data.Text as T
-- ---           ----------------------
import qualified Database.PostgreSQL.Simple as DS
-- ---           ----------------------
import           BmsLib.Types
import           GHC.Generics (Generic)
import           GHC.Int(Int64)

-- ---           ----------------------
--
toUpdateMap :: [BigMapUpdate] -> UpdateMap
toUpdateMap [] = Map.empty
toUpdateMap (x:xs) = case bmu_khash x of
  Nothing -> toUpdateMap xs
  Just kh -> Map.insertWith (+) (bmu_ptr x, kh) 1 (toUpdateMap xs)

--
{- ##### accessor ##### -}
-- | returns the total number of bigmaps.
getBigMapCount :: IO (Either String Int)
getBigMapCount = do
  (cc, bstr) <- curlGetString_ url_bigmap_count []
  return $ eitherDecode bstr

-- | returns all the big_maps updated greater or equals to given level.
getBigMaps :: Int -> IO (Either String [BigMap])
getBigMaps slv = do
  (cc, bstr) <- curlGetString_ (url_bigmaps slv) []
  return $ eitherDecode bstr

-- | returns a set of all the big_maps who is updated
--   greater or equals to the given level.
getBigMapSet :: Level -> IO (Either String BigMapSet)
getBigMapSet lv = do
  res <- getBigMapUpdates lv
  case res of
    Left str -> return $ Left str
    Right xs -> return $ Right $ Set.fromList $ Prelude.map bmu_ptr $ xs

-- | returns all updates for the given level
getBigMapUpdates :: Level -> IO (Either String [BigMapUpdate])
getBigMapUpdates lv = do
  (cc, bstr) <- curlGetString_ (urlBigmapUpdate lv) []
  return $ eitherDecode bstr

-- | returns all updates for the given level range
getBigMapUpdatesRange :: Level -> Level -> Offset -> ([BigMapUpdate] -> IO Int64) -> IO ()
getBigMapUpdatesRange start end offset f = do
  (cc, bstr) <- curlGetString_ (T.unpack $ urlBigmapUpdateRange start end offset) []
  case eitherDecode bstr of
    Left err -> putStrLn ("Offset: " <> show offset)
    Right [] -> putStrLn "Done!"
    Right bs -> 
      putStrLn ("fetch ..., offset: " <> show offset ) >>
      f bs >>
      putStrLn ("stored, offset: " <> show offset ) >>
      getBigMapUpdatesRange start end (offset + 1) f

-- | returns all updates for the given level
getBigMapUpdatesMap :: Level -> IO (Either String UpdateMap)
getBigMapUpdatesMap lv = do
  res <- getBigMapUpdates lv
  case res of
    Left str -> return $ Left str
    Right xs -> return $ Right $ toUpdateMap xs

-- | returns all "key_update" updates for the given level
get_big_map_updates_keyupdate :: Level -> IO (Either String [BigMapUpdate])
get_big_map_updates_keyupdate lv = do
  (cc, bstr) <- curlGetString_ (url_bigmap_update_key_update lv) []
  return $ eitherDecode bstr

-- | returns a map of all "key_update" updates for the given level
get_big_map_updates_map_keyupdate :: Level -> IO (Either String UpdateMap)
get_big_map_updates_map_keyupdate lv = do
  res <- get_big_map_updates_keyupdate lv
  case res of
    Left str -> return $ Left str
    Right xs -> return $ Right $ toUpdateMap xs

{- ##### JSON parser ##### -}
-- | big map
data BigMap = BigMap { ptr :: Int
                     , bmpath :: Maybe String
                     , firstLevel :: Int
                     , lastLevel :: Int
                     , totalKeys :: Int
                     , activeKeys :: Int
                     , updates :: Int
                     }
  deriving (Show, Eq, Ord)

instance FromJSON BigMap where
  parseJSON = withObject ""
    $ \o -> do
      p1 <- o .: "ptr"
      p2 <- o .:? "path"
      l1 <- o .: "firstLevel"
      l2 <- o .: "lastLevel"
      k1 <- o .: "totalKeys"
      k2 <- o .: "activeKeys"
      u <- o .: "updates"
      return $ BigMap p1 p2 l1 l2 k1 k2 u

-- | big map update
data BigMapUpdate = BigMapUpdate { bmu_id :: Int
                                 , bmu_lv :: Level
                                 , bmu_ptr :: BigMapPtr
                                 , bmu_action :: T.Text
                                 , bmu_address :: T.Text
                                 , bmu_khash :: Maybe KeyHash
                                 }
  deriving (Show, Eq, Ord, Generic)

instance DS.ToRow BigMapUpdate
instance FromJSON BigMapUpdate where
  parseJSON = withObject "bigmap update"
    $ \o -> do
      id <- o .: "id"
      level <- o .: "level"
      ptr <- o .: "bigmap"
      action <- o .: "action"
      content <- o .:? "content"
      hash <- case content of
        Nothing -> return Nothing
        Just p  -> withObject "content" (.:? "hash") p

      contract <- o .:? "contract"
      address <- case contract of
        Nothing -> return "NaN"
        Just p  -> withObject "content" (.: "address") p
      return $ BigMapUpdate id level ptr action address hash

{- ##### urls ##### -}
--
--
url_bigmap_count = "https://api.tzkt.io/v1/bigmaps/count"

--
url_bigmaps lv = "https://api.tzkt.io/v1/bigmaps"
  ++ "?lastLevel.ge="
  ++ (show lv)
  ++ "&limit=10000"

--
urlBigmapUpdate lv
  = "https://api.tzkt.io/v1/bigmaps/updates"
  <> "?level.eq="
  <> show lv
  <> "&limit=10000"

--
urlBigmapUpdateRange :: Level -> Level -> Offset -> T.Text
urlBigmapUpdateRange start end offset
  = "https://api.tzkt.io/v1/bigmaps/updates"
  <> "?level.ge="
  <> T.pack (show start)
  <> "&level.le="
  <> T.pack (show end)
  <> "&offset.pg="
  <> T.pack (show offset)
  <> "&limit=10000"

--
url_bigmap_update_key_update lv = (urlBigmapUpdate lv)
  ++ "&action.in=add_key,update_key"
--
