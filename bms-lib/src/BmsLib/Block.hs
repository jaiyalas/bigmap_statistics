{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ScopedTypeVariables #-}

--
module BmsLib.Block where
--
import           Control.Monad
-- ---           ----------------------
import           Data.Map.Strict as Map
import qualified Data.Set as Set
-- ---           ----------------------
import           Network.HTTP.Simple
import           Network.HTTP.Client
import           Network.Curl
-- ---           ----------------------
import           Data.Aeson
import           Data.Aeson.Types
-- ---           ----------------------
import           BmsLib.Types
--

baseURL lv =
  "https://api.tzkt.io/v1/operations/transactions"++
  "?limit=500"++
  "&level=" ++ (show lv)++
  "&status=applied"

getActiveContract :: Level -> IO [String]
getActiveContract lv = do
  (cc, bstr) <- curlGetString_ (baseURL lv) []
  case eitherDecode bstr of
    Left msg -> do
      putStrLn msg
      return []
    Right xs ->
      return $
        Prelude.filter (\x -> Prelude.take 2 x == "KT") $
        Prelude.map (tx_target) xs

data Tx = Tx
  { tx_target :: String
  } deriving (Show, Eq, Ord)

instance FromJSON Tx where
  parseJSON = withObject "" $ \o -> do
    (p1 :: String) <- (o .:  "type")
    p2 <- (o .:? "target")
    p3 <- case p2 of
             Nothing -> return Nothing
             Just p -> withObject "" (.:? "address") p
    case (p1, p3) of
      ("transaction", Just p) -> return $ Tx p
      _ -> error "parsing Tx failed"

