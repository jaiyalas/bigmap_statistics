{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ScopedTypeVariables #-}

--
module BmsLib.Contract where
--
import           Control.Monad
-- ---           ----------------------
import           Network.HTTP.Simple
import           Network.HTTP.Client
import           Network.Curl
-- ---           ----------------------
import           Data.Aeson
import           Data.Aeson.Types
import qualified Data.Text as T
import           Data.Text.Lazy (toStrict)
import           Data.Text.Lazy.Encoding (decodeUtf8)
-- ---           ----------------------
import qualified Database.PostgreSQL.Simple as DS
-- ---           ----------------------
import           BmsLib.Types
import           BmsLib.Tx
-- ---           ----------------------
import           GHC.Generics (Generic)
import           GHC.Int(Int64)
--
import           Debug.Trace
-- 
-- baseBigmapURL :: T.Text
-- baseBigmapURL = "https://api.tzkt.io/v1/bigmaps"
-- 
-- selectedField_bigmap :: [String]
-- selectedField_bigmap =
--   ["ptr"
--   ,"contract"
--   --
--   ,"firstLevel"
--   ,"lastLevel"
--   --
--   ,"totalKeys"
--   ,"activeKeys"
--   ,"updates"
--   --
--   ,"keyType"
--   ,"valueType"
--   ]
-- 
-- bigmapURL :: Offset -> [String] -> T.Text
-- bigmapURL offset fields
--   = baseBigmapURL
--   <> "?limit=10000"
--   <> "&active=true"
--   <> "&sort=ptr"
--   <> "&offset.pg="
--   <> T.pack (show offset)
--   <> "&select="
--   <> T.pack (foldr (\x hs -> x <> "," <> hs) "" fields)
-- 
-- getBigmapRange :: Offset
--                -> DBWriter BigmapRow
--                -> IO ()
-- getBigmapRange offset db_writer = do
--   let url = T.unpack $ bigmapURL offset selectedField_bigmap
--   (cc, bstr) <- curlGetString_ url []
--   case eitherDecode bstr of
--     Left err -> do
--       putStrLn ("API fetch failed at offset: " <> show offset)
--       putStrLn $ url
--       putStrLn err
--     Right [] -> putStrLn "Done!"
--     Right xs ->
--       putStr ("fetching offset:" <> show offset) >>
--       (db_writer $ map rowFromBigmap xs) >>
--       putStrLn (".. done(" <> show offset <> ")" ) >>
--       getBigmapRange (offset + 1) db_writer
-- 
-- -- #################################################
-- 
-- data Bigmap = Bigmap
--   { bm_ptr        :: Integer
--   , bm_contract   :: Maybe Address
--   --
--   , bm_firstLevel :: Integer
--   , bm_lastLevel  :: Integer
--   --
--   , bm_totalKeys  :: Integer
--   , bm_activeKeys :: Integer
--   , bm_updates    :: Integer
--   --
--   , bm_keyType    :: Value
--   , bm_valueType  :: Value
--   } deriving (Show, Eq, Ord, Generic)
-- 
-- instance FromJSON Bigmap where
--   parseJSON = genericParseJSON defaultOptions
--     {fieldLabelModifier = drop 3}
-- 
-- data BigmapRow = BigmapRow
--   { bmrow_ptr        :: Integer
--   , bmrow_contract   :: Maybe T.Text
--   --
--   , bmrow_firstLevel :: Integer
--   , bmrow_lastLevel  :: Integer
--   --
--   , bmrow_totalKeys  :: Integer
--   , bmrow_activeKeys :: Integer
--   , bmrow_updates    :: Integer
--   --
--   , bmrow_keyType    :: T.Text
--   , bmrow_valueType  :: T.Text
--   } deriving (Show, Eq, Ord, Generic)
-- 
-- instance DS.ToRow BigmapRow
-- 
-- rowFromBigmap :: Bigmap -> BigmapRow
-- rowFromBigmap bm =
--   BigmapRow
--   { bmrow_ptr        = bm_ptr bm
--   , bmrow_contract   = (bm_contract bm >>= kt_address)
--   --
--   , bmrow_firstLevel = bm_firstLevel bm
--   , bmrow_lastLevel  = bm_lastLevel bm
--   --
--   , bmrow_totalKeys  = bm_totalKeys bm
--   , bmrow_activeKeys = bm_activeKeys bm
--   , bmrow_updates    = bm_updates bm
--   --
--   , bmrow_keyType    = value2Text $ bm_keyType bm
--   , bmrow_valueType  = value2Text $ bm_valueType bm
--   }
-- 
-- 
