{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
--
module BmsLib.Head where
--
import           Control.Monad
-- ---           ----------------------
import           Network.HTTP.Simple
import           Network.HTTP.Client
import           Network.Curl
-- ---           ----------------------
import           Data.Aeson
import           Data.Aeson.Types
-- ---           ----------------------
--
-- # current head

base_url = "https://api.tzkt.io/v1/head"

data Head = Head
  { head_chain :: String
  , head_level :: Int
  , head_hash :: Maybe String
  , head_protocol :: Maybe String
  , head_timestamp :: String
  } deriving (Show, Eq)

instance FromJSON Head where
  parseJSON (Object o) =
    Head <$> (o .:  "chain")
         <*> (o .:  "level")
         <*> (o .:? "hash")
         <*> (o .:? "protocol")
         <*> (o .:  "timestamp")
  parseJSON _ = mzero

getHead :: IO (Either String Head)
getHead = do
  (cc, bstr) <- curlGetString_ base_url []
  return $ eitherDecode bstr

getHeadLevel :: IO Int
getHeadLevel = do
  res <- getHead
  return $ either (const 0) head_level res
