{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ScopedTypeVariables #-}

--
module BmsLib.Tx where
--
import           Control.Monad
-- ---           ----------------------
import           Network.HTTP.Simple
import           Network.HTTP.Client
import           Network.Curl
-- ---           ----------------------
import           Data.Aeson
import           Data.Aeson.Types
import qualified Data.Text as T
import           Data.Text.Lazy (toStrict)
import           Data.Text.Lazy.Encoding (decodeUtf8)
-- ---           ----------------------
import qualified Database.PostgreSQL.Simple as DS
-- ---           ----------------------
import           BmsLib.Types
import           GHC.Generics (Generic)
import           GHC.Int(Int64)
--
import           Debug.Trace

baseTxURL :: T.Text
baseTxURL = "https://api.tzkt.io/v1/operations/transactions"

selectedField :: [String]
selectedField =
  ["level"
  ,"hash"
  ,"sender"
  ,"target"
  ,"gasUsed"
  ,"storageUsed"
  ,"storageFee"
  ,"allocationFee"
  ,"parameter"
  ,"diffs"]

txURL :: Level -> Level -> Offset -> [String] -> T.Text
txURL start end offset fields
  = baseTxURL
  <> "?limit=5000"
  <> "&level.ge="
  <> T.pack (show start)
  <> "&level.le="
  <> T.pack (show end)
  <> "&offset.pg="
  <> T.pack (show offset)
  <> "&select="
  <> T.pack (foldr (\x hs -> x <> "," <> hs) "" fields)

getContractCallRange :: Level
                     -> Level
                     -> Offset
                     -> DBWriter ContractCallRow
                     -> IO ()
getContractCallRange start end offset db_writer = do
  let url = T.unpack $ txURL start end offset selectedField
  (cc, bstr) <- curlGetString_ url []
  case eitherDecode bstr of
    Left err -> do
      putStrLn ("API fetch failed at offset: " <> show offset)
      putStrLn err
    Right [] -> putStrLn "Done!"
    Right xs ->
      putStr ("fetching offset:" <> show offset) >>
      (db_writer $ concat $ map ccallFlatten $ xs) >>
      putStrLn ("done(" <> show offset <> ")" ) >>
      getContractCallRange start end (offset + 1) db_writer

-- #################################################

data TxParameter = TxParameter
  { tp_entrypoint :: Maybe T.Text
  , tp_value      :: Maybe T.Text
  } deriving (Show, Eq, Ord, Generic)

obj2Text :: Object -> T.Text
obj2Text = toStrict . decodeUtf8 . encode

value2Text :: Value -> T.Text
value2Text = T.pack . show . toJSON

instance FromJSON TxParameter where
  parseJSON = withObject "" $ \o -> do
      pe <- (o .:?  "entrypoint")
      (pv :: Maybe Value) <- (o .:?  "value")
      return $ TxParameter pe (fmap value2Text pv)

data DiffContent = DC
  { dc_hash  :: T.Text
  , dc_key   :: T.Text
  , dc_value :: Maybe T.Text
  } deriving (Show, Eq, Ord, Generic)

instance FromJSON DiffContent where
  parseJSON = withObject "" $ \o -> do
      ph <- (o .:  "hash")
      pk <- (o .:  "key")
      pv <- (o .:? "value")
      return $ DC ph (value2Text pk) (fmap value2Text pv)

data Diff = Diff
  { diff_bigmap :: Integer
  , diff_path   :: Maybe T.Text
  , diff_action :: T.Text
  , diff_content :: Maybe DiffContent
  } deriving (Show, Eq, Ord, Generic)

instance FromJSON Diff where
  parseJSON = genericParseJSON defaultOptions
    {fieldLabelModifier = drop 5}

data Address = KTAddress
  { kt_address :: Maybe T.Text
  } deriving (Show, Eq, Ord, Generic)

instance FromJSON Address where
    parseJSON = withObject "" $ \o -> do
      p <- (o .:? "address")
      return $ KTAddress p

data ContractCall = CCall
  { cc_level  :: Integer
  , cc_hash   :: T.Text
  , cc_sender :: Maybe Address
  , cc_target :: Maybe Address
  --
  , cc_gasUsed       :: Integer
  , cc_storageUsed   :: Integer
  , cc_storageFee    :: Integer
  , cc_allocationFee :: Integer
  --
  , cc_parameter     :: Maybe TxParameter
  , cc_diffs         :: Maybe [Diff]
  } deriving (Show, Eq, Ord, Generic)

instance FromJSON ContractCall where
  parseJSON = genericParseJSON defaultOptions
    {fieldLabelModifier = drop 3}

data ContractCallRow = CCallRow
  { ccrow_level  :: Integer
  , ccrow_hash   :: T.Text
  , ccrow_sender :: Maybe T.Text
  , ccrow_target :: Maybe T.Text
  --
  , ccrow_param_entry   :: Maybe T.Text
  , ccrow_param_value   :: Maybe T.Text
  --
  , ccrow_gasUsed       :: Integer
  , ccrow_storageUsed   :: Integer
  , ccrow_storageFee    :: Integer
  , ccrow_allocationFee :: Integer
  --
  , ccrow_diff_bigmap   :: Maybe Integer
  , ccrow_diff_path     :: Maybe T.Text
  , ccrow_diff_action   :: Maybe T.Text
  , ccrow_diffc_keyhash :: Maybe T.Text
  , ccrow_diffc_key     :: Maybe T.Text
  , ccrow_diffc_value   :: Maybe T.Text
  } deriving (Show, Eq, Ord, Generic)

instance DS.ToRow ContractCallRow
instance DS.FromRow ContractCallRow

-- #############################################

ccallFlatten :: ContractCall -> [ContractCallRow]
ccallFlatten cc =
  case cc_diffs cc of
    Just (y : ys) ->
      let step diff =
            CCallRow
            { ccrow_level  = cc_level cc
            , ccrow_hash   = cc_hash cc
            , ccrow_sender = (cc_sender cc >>= kt_address)
            , ccrow_target = (cc_target cc >>= kt_address)
            --
            , ccrow_gasUsed       = cc_gasUsed cc
            , ccrow_storageUsed   = cc_storageUsed cc
            , ccrow_storageFee    = cc_storageFee cc
            , ccrow_allocationFee = cc_allocationFee cc
            --
            , ccrow_param_entry   = (cc_parameter cc >>= tp_entrypoint)
            , ccrow_param_value   = (cc_parameter cc >>= tp_value)
            --
            , ccrow_diff_bigmap   = return $ diff_bigmap diff
            , ccrow_diff_path     = diff_path diff
            , ccrow_diff_action   = return $ diff_action diff
            , ccrow_diffc_keyhash = (diff_content diff >>= (return . dc_hash))
            , ccrow_diffc_key     = (diff_content diff >>= (return . dc_key))
            , ccrow_diffc_value   = (diff_content diff >>= dc_value)
            }
      in map step $ (y : ys)
    _ -> CCallRow
         { ccrow_level  = cc_level cc
         , ccrow_hash   = cc_hash cc
         , ccrow_sender = (cc_sender cc >>= kt_address)
         , ccrow_target = (cc_target cc >>= kt_address)
           --
         , ccrow_gasUsed       = cc_gasUsed cc
         , ccrow_storageUsed   = cc_storageUsed cc
         , ccrow_storageFee    = cc_storageFee cc
         , ccrow_allocationFee = cc_allocationFee cc
           --
         , ccrow_param_entry   = (cc_parameter cc >>= tp_entrypoint)
         , ccrow_param_value   = (cc_parameter cc >>= tp_value)
                                 --
         , ccrow_diff_bigmap   = Nothing
         , ccrow_diff_path     = Nothing
         , ccrow_diff_action   = Nothing
         , ccrow_diffc_keyhash = Nothing
         , ccrow_diffc_key     = Nothing
         , ccrow_diffc_value   = Nothing
         } : []
