{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
--
module BmsLib.Types where
--
import           Control.Monad
-- ---           ----------------------
import           Data.Map.Strict as Map
import qualified Data.Set as Set
-- ---           ----------------------
import           Network.HTTP.Simple
import           Network.HTTP.Client
import           Network.Curl
-- ---           ----------------------
import           Data.Aeson
import           Data.Aeson.Types
-- ---           ----------------------
import           GHC.Int(Int64)
--
type BigMapPtr = Int
type KeyHash = String
type Level = Int
type Offset = Int
--
type UpdateMap = Map (BigMapPtr, KeyHash) Int
type BigMapSet = Set.Set BigMapPtr
--
type Contract = String
--
type DBWriter a = [a] -> IO Int64
